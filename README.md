The resources contained here are for the Lethean Community, permission is granted to use and produce works for the benfit of the Lethean Community.

We ask that all brand guidelines are followed and our Logo is never used to show any endorsment of any kind.

Please contact us via email if you have any questions. contact@lethean.io

# Naming

Lethean project is called "Lethean VPN", the space is required when used publicly.

Short: lthn
Long : Lethean
Brand: Lethean VPN
Full: Lethean VPN - The Peoples Network.

# URLS

Website: https://lethean.io

Help documentation: https://lethean.help

Service Endpoint: https://lthn.io

Short URL Domain: https://lt.hn

# owned and Unused Domains

We have quite a few, some of them are not listed as they would expose in house developments.

- letheanvpn.com
- lethean.sh
- lethean.tool
- lethean.me
- lethean.id
- lethean.mx
- lethean.network

# Linking to Lethean

Please always try to use the project name in the paragraph used to link to us if the link test is not using "Lethean VPN", some examples are below;

## Rules
* never use "www"
* always link to https
* add rel="noopener"
* never link to us to imply endorcment unless you have cryptograhic proof & permission, all endorcments from our project come with a multi signature signed evedence. 

## Requests

### no rel="noreferer"

We use this to help us track where our traffic comes from, we take analytic privicay seriously and dont use any Hosted system. We use Open Web Analytics a open source Google Analytics alternative. It has the base features of Google Analytics before it got scary. 

We anon all IP's before they get saved dropping the last part, we dont link the data to anything, there is functionality to do dom streaming in OWA, however; this is not installed and we dont do this, it's creapy to track and record peoples mouse movements.

## Examples

### Link
```html
<a rel="noopener" alt="Lethean VPN Website" href="https://lethean.io">Lethean VPN</a> - The Peoples Network.
```

```html
Just found this Awesome VPN called Lethean, check them out: <a rel="noopener" href="https://lethean.io">The Peoples VPN Network.</a>.
It's actually VPN marketplace that let's people make their own webs of trust. 
Rather than paying a known company for privacy, I pay my Dad and my Uncle in America. 
My uncle secretly sends it back to me so i can buy my Mum her birthday gifts, she loves Willies Cacao.
```

