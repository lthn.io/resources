#!/usr/bin/env bash

SERVER='https://gitlab.com/'

mkdir -p lthn.io/projects/chain

git clone ${SERVER}lthn.io/projects/chain/lethean.git lthn.io/projects/chain/lethean
git clone ${SERVER}lthn.io/projects/chain/miner.git lthn.io/projects/chain/miner
git clone ${SERVER}lthn.io/projects/chain/wallet.git lthn.io/projects/chain/wallet

mkdir -p lthn.io/projects/sdk

git clone ${SERVER}lthn.io/projects/sdk/nodejs.git lthn.io/projects/sdk/nodejs
git clone ${SERVER}lthn.io/projects/sdk/build.git lthn.io/projects/sdk/build
git clone ${SERVER}lthn.io/projects/sdk/shell.git lthn.io/projects/sdk/shell

mkdir -p lthn.io/projects/vpn/extensions

git clone ${SERVER}lthn.io/projects/vpn/extensions/chrome-browser.git lthn.io/projects/vpn/extensions/chrome-browser
git clone ${SERVER}lthn.io/projects/vpn/extensions/firefox-browser.git lthn.io/projects/vpn/extensions/firefox-browser

git clone ${SERVER}lthn.io/projects/vpn/node.git lthn.io/projects/vpn/node


source ./clone-all.sh

SERVER='-l ' # default assumes local copy
#SERVER='https://gitlab.com/'

echo "Cloning the code"
mkdir -p ${USER}/projects/chain

git clone ${SERVER}lthn.io/projects/chain/lethean ${USER}/projects/chain/lethean
git clone ${SERVER}lthn.io/projects/chain/miner ${USER}/projects/chain/miner
git clone ${SERVER}lthn.io/projects/chain/wallet ${USER}/projects/chain/wallet

mkdir -p ${USER}/projects/sdk

git clone ${SERVER}lthn.io/projects/sdk/nodejs ${USER}/projects/sdk/nodejs
git clone ${SERVER}lthn.io/projects/sdk/build ${USER}/projects/sdk/build
git clone ${SERVER}lthn.io/projects/sdk/shell ${USER}/projects/sdk/shell

mkdir -p ${USER}/projects/vpn/extensions

git clone ${SERVER}lthn.io/projects/vpn/extensions/chrome-browser ${USER}/projects/vpn/extensions/chrome-browser
git clone ${SERVER}lthn.io/projects/vpn/extensions/firefox-browser ${USER}/projects/vpn/extensions/firefox-browser

git clone ${SERVER}lthn.io/projects/vpn/node ${USER}/projects/vpn/node

cd ${USER}/projects/vpn/node
git remote add gitlab https://gitlab.com/lthn.io/projects/vpn/node.git
git fetch gitlab
git checkout develop

cd ../extensions/firefox-browser
git remote add gitlab https://gitlab.com/lthn.io/projects/vpn/extensions/firefox-browser.git
git fetch gitlab
git checkout develop

cd ../chrome-browser
git remote add gitlab https://gitlab.com/lthn.io/projects/vpn/extensions/chrome-browser.git
git fetch gitlab
git checkout develop

cd ../../../sdk/shell
git remote add gitlab https://gitlab.com/lthn.io/projects/sdk/shell.git
git fetch gitlab
git checkout develop

cd ../build
git remote add gitlab https://gitlab.com/lthn.io/projects/sdk/build.git
git fetch gitlab
git checkout develop

cd ../nodejs
git remote add gitlab https://gitlab.com/lthn.io/projects/sdk/nodejs.git
git fetch gitlab
git checkout develop

cd ../../chain/lethean
git remote add gitlab https://gitlab.com/lthn.io/projects/chain/lethean.git
git fetch gitlab
git checkout develop

cd ../miner
git remote add gitlab https://gitlab.com/lthn.io/projects/chain/miner.git
git fetch gitlab
git checkout develop

cd ../wallet
git remote add gitlab https://gitlab.com/lthn.io/projects/chain/wallet.git
git fetch gitlab
git checkout develop

cd ../../../
echo "\n\n\n\ That Worked\n"
echo "Development checked out, work on this and push to gitlab remote."
